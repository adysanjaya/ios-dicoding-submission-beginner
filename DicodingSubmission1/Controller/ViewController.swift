//
//  ViewController.swift
//  DicodingSubmission1
//
//  Created by Kioser PC on 09/06/21.
//

import UIKit
import Lottie

class ViewController: UIViewController {
    
    final let url = URL(string: "https://beta.kioser.com/resep.json")
    @IBOutlet weak var recipeTableView: UITableView!
    var recipes: [Recipe]?
    var animationView : AnimationView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        recipeTableView?.isHidden = true
        
        // Menghubungkan heroTableView dengan ke dua metode di bawah
        recipeTableView.dataSource = self
        
        // Menghubungkan berkas XIB untuk HeroTableViewCell dengn heroTableView.
        recipeTableView.register(UINib(nibName: "RecipeTableViewCell", bundle: nil), forCellReuseIdentifier: "RecipeCell")
        
        //show loading
        animationView = AnimationView(name: "loading2")
        animationView?.frame = view.bounds
        animationView?.loopMode = .loop
        animationView?.animationSpeed = 0.5
        view.addSubview(animationView!)
        animationView?.play()
        
        // Menambahkan delegate ke table view
        recipeTableView.delegate = self
        
        //downloading data
        downloadJson()
    }
    
    func downloadJson(){
        guard let downloadURL = url else { return }
            
        URLSession.shared.dataTask(with: downloadURL){
            data,URLResponse, error in
            
            guard let data = data, error == nil, URLResponse != nil else {
                print("something is wrong")
                return
            }
            
            do
            {
                let decoder = JSONDecoder()
                let recipes = try decoder.decode(Recipes.self, from: data)
                self.recipes = recipes.recipe
                
                DispatchQueue.main.async {
                    self.recipeTableView.reloadData()
                    self.recipeTableView.isHidden = false
                    self.animationView?.isHidden = true
                    self.animationView?.play()
                }
            }
            catch {
                print("something is wrong after downloading \(error)")
            }
        }.resume()
    }
}

//adapter table
extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if(recipes?.count ?? 0) > 11 {
            count = 11
        }
        return count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeCell", for: indexPath) as? RecipeTableViewCell {
            
            if let recipe = recipes?[indexPath.row]{
                cell.titleRecipe.text = recipe.name
                cell.descRecipe.text = recipe.description

                if let checkImage = recipe.image, let imageURL = URL(string: checkImage){
                    DispatchQueue.global().async {
                        let data = try? Data(contentsOf: imageURL)
                        if let data = data {
                            let image = UIImage(data: data)
                            DispatchQueue.main.async {
                                cell.imageRecipe.image = image
                            }
                        }
                        else {
                            print("image not found on \(recipe.name) \(checkImage)")
                        }
                    }
                }
            }
            
            return cell
        } else {
            return UITableViewCell()
        }
    }

}

//ini aksi klik item tabel
extension ViewController: UITableViewDelegate {
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Memanggil View Controller dengan berkas NIB/XIB di dalamnya
        let detail = DetailViewController(nibName: "DetailViewController", bundle: nil)

        // Mengirim data hero
        detail.recipe = recipes?[indexPath.row]

        // Push/mendorong view controller lain
        self.navigationController?.pushViewController(detail, animated: true)
    }
}
