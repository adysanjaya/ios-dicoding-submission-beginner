//
//  RecipeTableViewCell.swift
//  DicodingSubmission1
//
//  Created by Kioser PC on 09/06/21.
//

import UIKit

class RecipeTableViewCell: UITableViewCell {

    @IBOutlet weak var titleRecipe: UILabel!
    @IBOutlet weak var descRecipe: UILabel!
    @IBOutlet weak var imageRecipe: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
