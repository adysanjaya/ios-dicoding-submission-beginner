//
//  DetailViewController.swift
//  DicodingSubmission1
//
//  Created by Kioser PC on 09/06/21.
//

import UIKit

class DetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var recipeTitle: UILabel!
    @IBOutlet weak var recipeDesc: UILabel!
    
    var recipe: Recipe?
    var groups: [(String, [String])] = []
    let CellIdentifier = "TableViewCell", HeaderViewIdentifier = "TableViewHeaderView"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Recipe Detail"
        self.recipeImage.layer.cornerRadius = 10
        self.recipeImage.layer.borderColor = UIColorFromRGB(0xFFFFFF).cgColor
        self.recipeImage.layer.masksToBounds = true
        self.recipeImage.contentMode = .scaleToFill
        self.recipeImage.layer.borderWidth = 3
        
        if let result = recipe {
            recipeTitle.text = result.name
            recipeDesc.text = result.description
            
            if let checkImage = result.image, let imageURL = URL(string: checkImage){
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: imageURL)
                    if let data = data {
                        let image = UIImage(data: data)
                        DispatchQueue.main.async {
                            self.recipeImage.image = image
                            // Do any additional setup after loading the view.
                        }
                    }
                }
            }
            
            if let ingredient = result.ingredient {
                
                var myValue : [String] = []
                var startCount : Int = 0
                
                for i in ingredient {
                    var fullText = ""
                    if let name = i.name, i.name != nil {
                        fullText+="\(name) "
                    }
                    
                    if let amount = i.amount, i.amount != nil {
                        fullText+="\(amount) "
                    }
                    
                    if let unit = i.unit, i.unit != nil {
                        fullText+="\(unit) "
                    }
                    
                    if(fullText.isEmpty == false){
                        startCount+=1
                        myValue.append("\(startCount). \(fullText)")
                    }
                }
                
                if myValue.isEmpty == false{
                    groups.append(("ingredients",myValue))
                }
                
            }
            
            if let step = result.step {
                
                var myValue : [String] = []
                
                for i in step {
                    if let description = i.description, i.description != nil {
                        myValue.append("- \(description)")
                    }
                }
                
                if myValue.isEmpty == false{
                    groups.append(("steps",myValue))
                }
            }
        }
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellIdentifier)
        tableView.register(UITableViewHeaderFooterView.self, forHeaderFooterViewReuseIdentifier: HeaderViewIdentifier)

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return groups.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groups[section].1.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath) as UITableViewCell
        let citiesInSection = groups[indexPath.section].1
        cell.textLabel?.text = citiesInSection[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: HeaderViewIdentifier)
        header?.textLabel?.text = groups[section].0
        return header
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func UIColorFromRGB(_ rgbValue: Int) -> UIColor! {
        return UIColor(
            red: CGFloat((Float((rgbValue & 0xff0000) >> 16)) / 255.0),
            green: CGFloat((Float((rgbValue & 0x00ff00) >> 8)) / 255.0),
            blue: CGFloat((Float((rgbValue & 0x0000ff) >> 0)) / 255.0),
            alpha: 1.0)
    }
}

