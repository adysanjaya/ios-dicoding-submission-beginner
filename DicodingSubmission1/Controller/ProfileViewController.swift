//
//  ProfileViewController.swift
//  DicodingSubmission1
//
//  Created by Kioser PC on 09/06/21.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var myPhoto: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        myPhoto.layer.cornerRadius = myPhoto.frame.size.width/2
    }
    
    @IBAction func fbAction(_ sender: UIButton) {
        if let url = URL(string: "https://web.facebook.com/ady.sanjaya.013") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func igAction(_ sender: UIButton) {
        if let url = URL(string: "https://www.instagram.com/ady_sanjaya013/") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func waAction(_ sender: UIButton) {
        if let url = URL(string: "https://api.whatsapp.com/send?phone=6282188480724&text=") {
            UIApplication.shared.open(url)
        }
    }
}
