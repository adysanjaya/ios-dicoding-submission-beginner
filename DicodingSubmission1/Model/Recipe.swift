//
//  Recipe.swift
//  DicodingSubmission1
//
//  Created by Kioser PC on 09/06/21.
//

import UIKit

class Recipes : Codable {
    
    let recipe : [Recipe]
    
    init(recipe : [Recipe]) {
        self.recipe = recipe
    }
    
}

class Recipe: Codable {
    let name : String
    let image : String?
    let description : String
    let notes : String?
    let ingredient : [Ingredient]?
    let step : [Step]?
    
    init(name:String, image:String, description: String, notes: String?,ingredient : [Ingredient]?,step : [Step]?) {
        self.name = name
        self.image = image
        self.description = description
        self.notes = notes
        self.ingredient = ingredient
        self.step = step
    }
}
