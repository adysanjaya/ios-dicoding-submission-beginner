//
//  Ingredient.swift
//  DicodingSubmission1
//
//  Created by Kioser PC on 09/06/21.
//

import Foundation

class Ingredient: Codable {
    let amount : String?
    let unit : String?
    let name : String?
    let preparation : String?
}
